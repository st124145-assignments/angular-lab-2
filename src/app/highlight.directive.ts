import { Directive, HostListener, ElementRef, Renderer2 } from '@angular/core';


@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostListener('mouseenter') onMouseEnter() {
    // Handle mouse enter event (e.g., change element's style)
    this.renderer.setStyle(this.el.nativeElement, 'background-color', 'yellow');
  }

  @HostListener('mouseleave') onMouseLeave() {
    // Handle mouse leave event (e.g., reset element's style)
    this.renderer.removeStyle(this.el.nativeElement, 'background-color');
  }
}
