import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent {

  // Custom validator to check if password and confirm password match
  passwordMatchValidator(form: NgForm) {
    const password = form.value.password;
    const confirmPassword = form.value.confirmPassword;
  //   if (password !== confirmPassword) {
  //     form.controls.[confirmPassword].setErrors({ passwordMismatch: true });
  //   } else {
  //     form.controls.[confirmPassword].setErrors(null);
  //   }
  }
  // user: User = new User(username,email,password,confirmPassword);
}
// export class User {
//   username: string;
//   email: string;
//   password: string;
//   confirmPassword: string;
//   constructor(username:string, email:string, password:string, confirmPassword:string){
//     this.username=username;
//     this.email=email;
//     this.password=password;
//     this.confirmPassword=confirmPassword;
//   }
// }
