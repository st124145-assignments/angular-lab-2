import { Component } from '@angular/core';

@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.css']
})
export class Task3Component {
  countKey = 0;

  onClick(){
    console.log('Button clicked');
  }

  count(event: any){
    this.countKey += 1;
  }
}
