import { Directive, HostListener, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appDisableOnClick]'
})
export class DisableOnClickDirective {
  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostListener('click', ['$event']) onClick(event: Event) {
    // Disable the button
    this.renderer.setProperty(this.el.nativeElement, 'disabled', true);
  }
}
