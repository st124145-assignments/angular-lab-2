import { Component } from '@angular/core';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.css']
})
export class NgforComponent {
  studentList = [
    {
      name: 'Minn',
      program: 'CS'
    },
    {
      name: 'Kyi',
      program: 'CSIM & DSAI'
    }
  ]
}
